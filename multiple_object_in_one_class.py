class Employee:
    raise_amt=1.04
    def __init__(self,first,last,pay):
        self.first=first
        self.last=last
        self.email=first+'.'+last+'@email.com'
        self.pay=pay
    def fulname(self):
        return '{} {}'.format(self.first,self.last)
    def applay_raise(self):
            self.pay=int(self.pay+self.raise_amt)
    def __str__(self):
        return self.first+" "+self.last
    def __len__(self):
        return len(self.first)
    def __add__(s1,s2):
        return s1.pay+s2.pay

dev_1=Employee('ali','raza',5000)
dev_2=Employee('ali','raza',5000)
result=dev_1+dev_2
print(result)
dev_2=Employee('test','adnan',1000)
print(dev_1.email)
print(dev_2.email)
dev_1.applay_raise()
print(dev_1.pay)
input()

##This exampale shows that multiple objects be created from the same class.

