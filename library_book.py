import logging
from odoo import models, fields,api
from odoo.exceptions import ValidationError
from datetime import timedelta
from odoo.exceptions import UserError
from odoo.tools.translate import _


logger = logging.getLogger(__name__)

class BaseArchive(models.AbstractModel):
    _name = 'base.archive'
    active = fields.Boolean(default=True)



class LibraryBook(models.Model):

    _name = 'library.book'
    _inherit = ['base.archive']
    _description = 'Library Book'
    _order = 'date_release desc'
    _rec_name = 'name'

    name = fields.Char('Title', required=True)
    short_name = fields.Char('Short Title', translate=True, index=True)
    notes = fields.Text('Internal Notes')
    state = fields.Selection(
        [('draft', 'Not Available'),
         ('available', 'Available'),
         ('lost', 'Lost'),
         ('borrowed','Borrowed')],
        'State', default="draft")
    description = fields.Html('Description', sanitize=True, strip_style=False)
    cover = fields.Binary('Book Cover')
    out_of_print = fields.Boolean('Out of Print?')
    date_release = fields.Date('Release Date')
    date_updated = fields.Datetime('Last Updated')
    pages = fields.Integer('Number of Pages',
                           groups='base.group_user',
                           states={'lost': [('readonly', True)]},
                           help='Total book page count',
                           company_dependent=False)
    reader_rating = fields.Float('Reader Average Rating',
                                 digits=(14, 4),)
    date_release = fields.Date('Release Date')
    cost_price = fields.Float('Book Cost',
                              digits='Book Price')
    author_ids = fields.Many2many('res.partner',
                                  string='Authors')
    currency_id = fields.Many2one('res.currency',
                                  string='Currency')
    retail_price = fields.Monetary('Retail Price')
    publisher_id = fields.Many2one('res.partner',
                                   string='Publisher',
                                   ondelete='set null',
                                   context={},
                                   domain=[], )
    publisher_city = fields.Char('publisher city',
                                 related='publisher_id.city',
                                 readonly=True)
    category_id = fields.Many2one('library.book.category')
    age_days = fields.Float(string='Days since release',
                            compute='_compute_age',
                            inverse='_inverse_age',
                            search='_search_age')

    @api.depends('date_release')
    def _compute_age(self):
        today=fields.Date.today()
        for book in self.filtered('date_release'):
            delta = today - book.date_release
            book.age_days = delta.days

    def _inverse_age(self):
        today = fields.Date.today()
        for book in self:
            if book.date_release:
                delta = today - book.date_release
                book.age_days = delta.days
            else:
                book.age_days = 0

    def _search_age(self,oprator,value):
        today = fields.Date.today()
        value_days = timedelta(days=value)
        value_date = today - value_days
        oprator_map = {'>': '<','>=':'<=',
                       '<':'>','<=':'>=',}
        new_op = oprator_map.get(operator,oprator)
        return [('date_release',new_op,value_date)]

    _sql_constraints=[('name_uniq','UNIQUE (name)','Book title must be unique.')]

    @api.constrains('date_release')
    def _check_release_date(self):
        for record in self:
            if record.date_release and record.date_release > fields.Date.today():
                raise models.ValidationError('Release date must be in the past')

    ref_doc_id = fields.Reference(selection=[('res.users', 'User'), ('res.partner', 'Partner'), ('library.book.category','categ')], string='Reference Document')

    @api.model
    def _referencable_models(self):
        models = self.env['ir.model'].search([('field_id.name', '=', 'message_ids')])
        return [(x.model, x.name) for x in models]
    @api.model
    def is_allowed_transition(self,old_state,new_state):
        allowed = [('draft','available'),
                   ('available', 'borrowed'),
                   ('borrowed', 'available'),
                   ('available', 'lost'),
                   ('borrowed', 'lost'),
                   ('lost', 'available')]
        return (old_state, new_state) in allowed

    def change_state(self, new_state):
        for book in self:
            if book.is_allowed_transition(book.state, new_state):
                book.state = new_state
            else:
                msg = _('Moving from %s to %s is not allowed') % (book.state, new_state)
                raise UserError(msg)

    def make_available(self):
        self.change_state('available')
    def make_borrowed(self):
        self.change_state('borrowed')
    def make_lost(self):
        self.change_state('lost')

    def log_all_library_members(self):
        library_member_model = self.env['library.members']
        all_members = library_member_model.search([])
        print("ALL MEMBERS:", all_members)
        return True

    def create_categories(self):
        categ1 = {
            'name': 'Child category 1',
            'description': 'Description for child 1'
        }
        categ2 = {
            'name': 'Child category 2',
            'description': 'Description for child 2'
        }
        parent_category_val = {
            'name': 'Parent category',
            'description': 'Description for parent category',
            'child_ids': [
                (0, 0, categ1),
                (0, 0, categ2),
            ]
        }

        record = self.env['library.book.category'].create(parent_category_val)
        return True

    def change_release_date(self):
        self.ensure_one()
        self.date_release = fields.Date.today()
    def find_book(self):
        domain = [
            '|',
                '&', ('name', 'ilike', 'Book Name'),
                     ('category_id.name', 'ilike', 'Category Name'),
                '&', ('name', 'ilike', 'Book Name 2'),
                     ('category_id.name', 'ilike', 'Category Name 2')
        ]
        books = self.search(domain)
        logger.info('Books found: %s', books)
        return True


class LibraryMember (models.Model):
    _name = 'library.members'
    _inherits = {'res.partner' : 'partner_id'}
    partner_id = fields.Many2one('res.partner',ondelete='cascade')
    date_start = fields.Date('Member Since')
    date_end =fields.Date('Termination Date')
    member_number = fields.Char()
    date_of_birth= fields.Date('Date of birth')


class ResPartner(models.Model):
    _inherit = 'res.partner'
    _order = 'name'
    published_book_ids = fields.One2many('library.book', 'publisher_id', string='Published Books')
    authored_book_ids = fields.Many2many('library.book', string='Authored Books')
    count_books = fields.Integer('Number of Authored Books', compute='_compute_count_books')
    @api.depends('authored_book_ids')
    def _compute_count_books(self):
        for r in self:
            r.count_books = len(r.authored_book_ids)


