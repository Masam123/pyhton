class Bug:
   def __init__(self):
       self.wings = 4
       self.ears = 0

class Human:
   def __init__(self):
       self.legs = 2
       self.arms = 2
       self.eyes = 2

bob = Human()
tom = Bug()

print(tom.wings)
print(bob.arms)
print(tom.ears)
print(bob.eyes)
input()
