def print_msg(number):
    def printer():
        "Here we are using the nonlocal keyword"
        nonlocal number
        number=3
        print(number)
    printer()
    print(number)

print_msg(0)
#2. Can functions be called inside a function?
# yes functions be called inside a function
