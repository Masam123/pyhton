class Bug:
   def __init__(self):
       self.wings = 4
       self.eyes = 2

class Human:
   def __init__(self):
       self.legs = 2
       self.arms = 2

bob = Human()
tom = Bug()
joy = Human()
jerry = Bug()

print(tom.wings)
print(bob.arms)
print(joy.arms)
print(jerry.eyes)
input()
