#There is no limit to the number of classes you can inherit
class Human:
    name = ""

class Coder:
    skills = 3

class age:
    personage=20

class height:
    personheight=5.6
    
class Pythonista(Human, Coder,age,height):
    version = 3

obj = Pythonista()
obj.name = "Alice"

print(obj.name)
print(obj.version)
print(obj.skills)
print(obj.personage)
print(obj.personheight)
