class Parent:
    parentname = ""
    childname = ""
 
    def show_parent(self):
        print(self.parentname)
 
 
# Son class inherits Parent class
class Son(Parent):
    def show_child(self):
        print(self.childname)
 
 
# Daughter class inherits Parent class
class Daughter(Parent):
    def show_child(self):
        print(self.childname)
 
 
s1 = Son()  # Object of Son class
s1.parentname = "Mark"
s1.childname = "John"
s1.show_parent()
s1.show_child()
 
s1 = Daughter()  # Object of Daughter class
s1.childname = "Riya"
s1.parentname = "Samule"
s1.show_parent()
s1.show_child()
