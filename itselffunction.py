##Recursion is a way of programming or coding a problem, in which a function
##calls itself one or more times in its body.
##Usually, it is returning the return value of this function call.
##If a function definition fulfils the condition of recursion,
##we call this function a recursive function.
##Exampal
def factorial(n):
    print("factorial has been called with n = " + str(n))
    if n == 1:
        return 1
    else:
        res = n * factorial(n-1)
        print("intermediate result for ", n, " * factorial(" ,n-1, "): ",res)
        return res	

print(factorial(5))
