def replaceMultiple(mainString, toBeReplaces, newString):
   
    for elem in toBeReplaces :
        
        if elem in mainString :
            
            mainString = mainString.replace(elem, newString)
    
    return  mainString     
 
def main():
    
    mainStr = "Hello, This is a sample string"
 
   
    otherStr = mainStr.replace('s' , 'X') 
     
    print("String with replaced Content : " , otherStr) 

    otherStr = mainStr.replace('s' , 'XXXS',2) 
     
    print(otherStr) 
    
    
 
     
    otherStr = replaceMultiple(mainStr, ['s', 'l', 'a'] , "AA")
    
    print(otherStr)
           
if __name__ == '__main__':
    main()
