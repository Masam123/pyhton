def FileCheck(fn):
    try:
      open(fn, "r")
      return 1
    except IOError:
      print ("Error: File does not appear to exist.")
      return 0

result = FileCheck("testfile")
print (result)
#yes try-except catch the error if a file can’t be opened
